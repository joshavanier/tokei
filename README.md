[![Screenshot](screenshot.png)](https://tokei.avanier.now.sh)

**Tokei** is a just a clock. Use it as your home or tab page.

## Features

Press <kbd>M</kbd> to toggle between decimal, 12h, and 24h modes.

Add a hash to the URL to customise the page; provide two hexadecimal colour codes separated by a dash for the background and foreground:

    #030303-e4e4e4

Press <kbd>I</kbd> to invert the UI colours.
---

**By [Avanier](https://avanier.now.sh). Licensed under [MIT](LICENSE).**
