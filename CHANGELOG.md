**Changelog**

## 1.1.0 - 7R19

### +
- Added a decimal clock mode
- Press <kbd>I</kbd> to invert UI colours

### *
- Instead of clicking on the screen, press <kbd>M</kbd> to toggle modes
- Colour configs are now saved locally

## 1.0.0 - 7Q19

### +
- Initial release
