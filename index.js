'use strict';

const pad = num => num > 9 ? num : `0${num}`;
const clock = document.getElementById('c');
const lsKey = 'tokei';
const config = Object.prototype.hasOwnProperty.call(localStorage, lsKey)
  ? JSON.parse(localStorage.getItem(lsKey))
  : {bg: '#030303', fg: '#e4e4e4', mode: 0};

function decimal (date = new Date()) {
  const t = (new Date(date) - date.setHours(0, 0, 0, 0)) / 864E5;
  return t.toFixed(6).substr(2, 3);
}

function H24 (date = new Date()) {
  return `${pad(date.getHours())}:${pad(date.getMinutes())}`;
}

function H12 (date = new Date()) {
  let hour = date.getHours();
  const period = hour < 12 ? 'AM' : 'PM';
  hour %= 12;
  hour = hour === 0 ? 12 : hour;
  const minutes = date.getMinutes();
  return `${pad(hour)}:${pad(minutes)} ${period}`;
}

function displayTime ({mode} = config) {
  let time = '';
  switch (mode) {
    case 1: time = H12(); break;
    case 2: time = H24(); break;
    default: time = decimal(); break;
  }
  document.title = time;
  clock.innerHTML = time;
}

function save () {
  localStorage.setItem(lsKey, JSON.stringify(config));
}

function toggle () {
  config.mode = config.mode >= 2 ? 0 : config.mode + 1;
  displayTime();
  save();
}

function paint ({bg, fg} = config) {
  Object.assign(document.body.style, {
    backgroundColor: bg, color: fg
  });
}

function invert () {
  const {backgroundColor, color} = document.body.style;
  Object.assign(config, {bg: color, fg: backgroundColor});
  save();
  paint();
}

function parseHash () {
  const isHex = value => /^#[0-9a-f]{3}(?:[0-9a-f]{3})?$/i.test(value);
  const {hash} = window.location;
  if (hash) {
    let [bg, fg] = hash.split('-');
    fg = `#${fg}`;
    if (isHex(bg) && isHex(fg)) {
      Object.assign(config, {bg, fg});
      save();
    }
  }
  paint();
}

window.onhashchange = parseHash;

document.onkeyup = ({code}) => {
  if (code === 'KeyI') invert();
  else if (code === 'KeyM') toggle();
};

parseHash();
displayTime();
setInterval(displayTime, 1E3);
